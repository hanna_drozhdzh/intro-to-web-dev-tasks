import { buttonsDiscover, images, titleDiscover } from "../../api/api";

export function createImage() {
  const img = document.createElement("img"); // eslint-disable-line no-undef
  img.classList.add("discover__image");
  img.setAttribute("src", images[3].discoverImage);

  return img;
}

export function createButtonWrapper(items) {
  const wrapper = document.createElement("div"); // eslint-disable-line no-undef
  wrapper.classList.add("button-wrapper");

  items.forEach(item => {
    const button = document.createElement("button"); // eslint-disable-line no-undef
    button.classList.add("purple-button", "discover__button");
    button.innerHTML = item.buttonText;
    wrapper.append(button);
  });
  return wrapper;
}

export function createDescription() {
  const desc = document.createElement("p"); // eslint-disable-line no-undef
  desc.classList.add("discover__description");
  desc.innerHTML = titleDiscover[1].description;

  return desc;
}

export function createTitle() {
  const titlee = document.createElement("h1"); // eslint-disable-line no-undef
  titlee.classList.add("discover__title");
  titlee.innerHTML = titleDiscover[0].mainTitle;

  return titlee;
}

export function createTitleWrapper() {
  const titleWrapper = document.createElement("div"); // eslint-disable-line no-undef
  titleWrapper.classList.add("main-content");

  const title = createTitle();

  const description = createDescription();

  const buttonsWrapper = createButtonWrapper(buttonsDiscover);

  titleWrapper.append(title, buttonsWrapper, description);

  return titleWrapper;
}

export function renderDiscover() {
  // eslint-disable-next-line no-undef
  const main = document.querySelector(".main");

  const titleWrapper = createTitleWrapper();

  const img = createImage(images);

  main.replaceChildren(titleWrapper, img);
}
