import { images, signUpFormLabels, title } from "../../api/api";  

export function createGirlImage() {
  const img = document.createElement("img"); // eslint-disable-line no-undef
  img.classList.add('signUp__girl-image');
  img.src = images[2].signUpBg;
  
  return img;
}

export function createButton() {
    const titlee = document.createElement("button"); // eslint-disable-line no-undef
    titlee.className = "purple-button";
    titlee.innerHTML = title[2].buttonText;
  
    return titlee;
  }

export function createForm(items) {
    // eslint-disable-next-line no-undef
    const form = document.createElement('form');
    form.classList.add('registration-form');
    
    items.forEach(item => {
        // eslint-disable-next-line no-undef
        const label = document.createElement('label');
        label.innerHTML = item.label;
        // eslint-disable-next-line no-undef
        const input = document.createElement('input');
        label.append(input);
        form.append(label);
    });
    return form;
}

export function createContentWrapper() {
  const mainContent = document.createElement("div"); // eslint-disable-line no-undef
  mainContent.classList.add("signUp__main-content");

  const form = createForm(signUpFormLabels); 
  
  const button = createButton(title);

  mainContent.append(form, button);

  return mainContent;
}

export function renderSignUp() {
    // eslint-disable-next-line no-undef
    const main = document.querySelector(".main");
    main.classList.add('main-content')
    // eslint-disable-next-line no-undef
    const mainContent = createContentWrapper();
    const img = createGirlImage(images)
    main.replaceChildren(mainContent, img);
  }