import { images, navBarItemsHeader } from "../api/api";

export function createNav(items) {
  const nav = document.createElement("nav"); // eslint-disable-line no-undef
  const ul = document.createElement("ul"); // eslint-disable-line no-undef

  nav.className = "header__nav";
  ul.className = "header__menu";

  items.forEach(item => {
    const li = document.createElement("li"); // eslint-disable-line no-undef
    const a = document.createElement("a"); // eslint-disable-line no-undef
    a.innerHTML = item.title;
    a.setAttribute("href", `/${item.title}`);
    a.className = "header__menu__item";
    li.append(a);
    ul.append(li);
  });

  nav.append(ul);
  return nav;
}

export function createLogoText() {
  const logoText = document.createElement("p"); // eslint-disable-line no-undef
  logoText.innerHTML = images[0].title;
  logoText.className = "header__logo-text";
  return logoText;
}

export function createLogoIcon() {
  const logoIcon = document.createElement("img"); // eslint-disable-line no-undef
  logoIcon.setAttribute("src", images[0].src);
  logoIcon.className = "header__logo-icon";
  return logoIcon;
}

export function createLogo() {
  const logo = document.createElement("div"); // eslint-disable-line no-undef

  logo.className = "logo";

  const logoIcon = createLogoIcon(images);

  const logoText = createLogoText(images);

  logo.append(logoIcon, logoText);

  return logo;
}

export function createHeader() {
  const header = document.createElement("header"); // eslint-disable-line no-undef
  header.className = "header";

  const nav = createNav(navBarItemsHeader);

  const logo = createLogo(images);

  header.append(logo, nav);

  return header;
}

export function setupHeader(parent) {
  const header = createHeader();
  parent.append(header);
}
