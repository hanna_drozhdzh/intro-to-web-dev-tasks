import { navBarItemsFooter } from "../api/api";

export function createNav(items) {
  const nav = document.createElement("nav"); // eslint-disable-line no-undef
  const ul = document.createElement("ul"); // eslint-disable-line no-undef

  nav.className = "footer__nav";
  ul.className = "footer__menu";

  items.forEach(item => {
    const li = document.createElement("li"); // eslint-disable-line no-undef
    li.innerHTML = item.title;
    li.className = "footer__menu__item";
    ul.append(li);
  });

  nav.append(ul);
  return nav;
}

export function createFooter() {
  const footer = document.createElement("footer"); // eslint-disable-line no-undef
  footer.className = "footer";

  const nav = createNav(navBarItemsFooter);

  footer.append(nav);

  return footer;
}

export function setupFooter(parent) {
  const footer = createFooter();
  parent.append(footer);
}
