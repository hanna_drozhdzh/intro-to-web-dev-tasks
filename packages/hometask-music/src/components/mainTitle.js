import { images, title } from "../api/api";
import { renderSignUp } from "./header-navBars/join";

export function createGirlImage() {
  const img = document.createElement("img"); // eslint-disable-line no-undef
  img.className = "homePage__girl-image";
  img.src = images[1].homeBg;
  
  return img;
}

export function createButton() {
  const button = document.createElement("button"); // eslint-disable-line no-undef
  button.className = "purple-button";
  button.innerHTML = title[2].buttonText;
  button.addEventListener('click', renderSignUp);

  return button;
}

export function createDescription() {
  const desc = document.createElement("p"); // eslint-disable-line no-undef
  desc.className = "main__title__description";
  desc.innerHTML = title[1].description;

  return desc;
}

export function createTitle() {
  const titlee = document.createElement("h1"); // eslint-disable-line no-undef
  titlee.className = "main__title";
  titlee.innerHTML = title[0].mainTitle;

  return titlee;
}

export function createContentWrapper() {
  const mainContent = document.createElement("div"); // eslint-disable-line no-undef
  mainContent.classList.add("main-content");

  const titlee = createTitle();

  const button = createButton();

  const description = createDescription();

  mainContent.append(titlee, description, button);

  return mainContent;
}

export function createMain() {
  const main = document.createElement("main"); // eslint-disable-line no-undef
  main.className = "main";

  const mainContent = createContentWrapper();

  const girlImg = createGirlImage(images);

  main.append(mainContent, girlImg);

  return main;
}

export function setupMain(parent) {
  const main = createMain();
  parent.append(main);
}
