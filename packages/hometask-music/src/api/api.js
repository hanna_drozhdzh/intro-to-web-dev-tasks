import logo from "../../assets/icons/logo.svg";
import homeGirlImage from "../../assets/images/background-page-landing.png";
import signupGirlImage from "../../assets/images/background-page-sign-up.png";
import musicTitles from "../../assets/images/music-titles.png";

export const navBarItemsHeader = [
  {
    title: "Discover",
  },
  {
    title: "Join",
  },
  {
    title: "Sign in",
  },
];

export const images = [
  {
    src: logo,
    title: "Simo",
  },
  {
    homeBg: homeGirlImage,
  },
  {
    signUpBg: signupGirlImage,
  },
  {
    discoverImage: musicTitles,
  },
];

export const title = [
  {
    mainTitle: "Feel the music",
  },
  {
    description: "Stream over 10 million songs with one click",
  },
  {
    buttonText: "Join now",
  },
];

export const titleDiscover = [
  {
    mainTitle: "Discover new music",
  },
  {
    description:
      "By joing you can benefit by listening to the latest albums released",
  },
];

export const buttonsDiscover = [
  {
    buttonText: "Charts",
  },
  {
    buttonText: "Songs",
  },
  {
    buttonText: "Artists",
  },
];

export const signUpFormLabels = [
  {
    label: 'Name:',
  },
  {
    label: 'Password:',
  },
  {
    label: 'e-mail:',
  },
];

export const navBarItemsFooter = [
  {
    title: "About us",
  },
  {
    title: "Contact",
  },
  {
    title: "CR Info",
  },
  {
    title: "Twitter",
  },
  {
    title: "Facebook",
  },
];