import { setupFooter } from "./components/footer";
import { setupHeader } from "./components/header";
import { setupMain } from "./components/mainTitle";
import { renderDiscover } from "./components/header-navBars/discover"
import { renderSignUp } from "./components/header-navBars/join";

function setupHome() {
    // eslint-disable-next-line no-undef
    setupHeader(document.getElementById("app"));
    
    // eslint-disable-next-line no-undef
    setupMain(document.getElementById("app"));
    
    // eslint-disable-next-line no-undef
    setupFooter(document.getElementById("app"));
    };

const routes = {                   
  "/Discover": renderDiscover,
  "/Join": renderSignUp,
};

function renderRoute(e) {
  e.preventDefault();

  if (e.target.pathname === "/Discover" && window.location.pathname === "/Join") {
    const app = document.getElementById('app');
    app.innerHTML = '';
    setupHome();
  } else {
    if (e.target.href) {
      routes[e.target.pathname]();
      history.pushState({}, "", e.target.pathname);
      e.target.classList.add("hidden");
    }
  }
}

function start() {
  setupHome();
  // eslint-disable-next-line no-undef
  const mainMenu = document.querySelector(".header__nav");

  mainMenu.addEventListener("click", renderRoute);
}

start();