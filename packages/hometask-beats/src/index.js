function openDropdownMenu() {
  const menu = document.getElementById("dropdown-menu");
  const burgerButton = document.getElementById("burger-button");
  menu.classList.add("show");
  burgerButton.classList.add("hidden-on-mobile");
}

const burgerButton = document.getElementById("burger-button");
burgerButton.addEventListener("click", openDropdownMenu);

function closeDropdownMenu() {
  const menu = document.getElementById("dropdown-menu");
  menu.classList.remove("show");

  const burgerButton = document.getElementById("burger-button");
  burgerButton.classList.remove("hidden-on-mobile");
}

const crossButton = document.getElementById("cross-button");
crossButton.addEventListener("click", closeDropdownMenu);

import Swiper, { Navigation } from "swiper";
import "swiper/css";
import "swiper/css/navigation";

const swiper = new Swiper(".swiper", {
  modules: [Navigation],
  slidesPerView: 1,
  breakpoints: {
    550: {
      slidesPerView: 2,
    },
    768: {
      slidesPerView: 3,
    },
  },
  loop: true,
  spaceBetween: 20,
  centeredSlides: true,
  navigation: {
    nextEl: ".slide-next",
    prevEl: ".slide-prev",
  },
});
